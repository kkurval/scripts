$AADConnectDLUrl="https://files.rangeforce.com/labs/mpp/public/AzInfoProtection.exe"

$tempfile = [System.IO.Path]::GetTempFileName()
$folder = [System.IO.Path]::GetDirectoryName($tempfile)

$webclient = New-Object System.Net.WebClient
$webclient.DownloadFile($AADConnectDLUrl, $tempfile)

Rename-Item -Path $tempfile -NewName "AzInfoProtection.exe"
$MSIPath = $folder + "\AzInfoProtection.exe"

Invoke-Expression "& `"$MSIPath`" /install /quiet /norestart /log C:\Emuldata\AIP_Install.log"