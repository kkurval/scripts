cd C:\Emuldata\
# iwr https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.4.0-Windows-msi.zip -OutFile VNCServer.zip
iwr http://192.168.0.254/VNC-Server-6.4.0-Windows-msi.zip -OutFile VNCServer.zip
[System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
[System.IO.Compression.ZipFile]::ExtractToDirectory("C:\Emuldata\\VNCServer.zip", "C:\Emuldata\VNCServer")
cd .\VNCServer\
# RUN AS ADMIN
# Opens extra conf window post install. Can be ignored.
start powershell (IEX (iwr https://bitbucket.org/kkurval/scripts/raw/1ee67f6e0201431d8b7ea9b2d303e54237ff0aff/Install_VNCServer2.ps1 -usebasicparsing)) -PassThru | select -ExpandProperty id
# Give time for installation to finish
sleep 15
kill $id -ea 0

cmd /c sc config "vncserver" start= demand
cmd /c sc sdset vncserver "D:(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRCRPWP;;;IU)(A;;CCLCSWLOCRRC;;;SU)"
cmd /c sc stop "vncserver"
cmd /c sc config "vncserver" type=interact type= share
cmd /c sc stop "vncserver"