$PSVersionTable
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/service/auth '@{Basic = true}'
winrm set winrm/config/client '@{AllowUnencrypted="true"}'
winrm set winrm/config/client/auth '@{Basic = true}'
#Set-Item WSMan:\localhost\Client\TrustedHosts -Value "*" -Force
Set-Item WSMan:\localhost\Client\TrustedHosts -Value "192.168.0.254" -Force
Enable-PSRemoting -SkipNetworkProfileCheck -Force

Get-Service winrm | fl
winrm get winrm/config/Service |findstr /i AllowUnencrypted
winrm GET winrm/config/client | findstr /i truste
winrm get winrm/config/Service | findstr /i basic
winrm get winrm/config/client | findstr /i basic