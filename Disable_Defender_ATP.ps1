Function Disable-Defender {

	# Telemetry related
	Set-MpPreference -MAPSReporting 2
	Set-MpPreference -SubmitSamplesConsent 3
	#Set-MpPreference -ThreatIDDefaultAction_Actions NoAction
	# Action
	Set-MpPreference -LowThreatDefaultAction NoAction
	Set-MpPreference -HighThreatDefaultAction NoAction
	Set-MpPreference -SevereThreatDefaultAction NoAction
	Set-MpPreference -UnknownThreatDefaultAction NoAction
	# Disabled features
	Set-MpPreference -DisableScanningMappedNetworkDrivesForFullScan $true
	Set-MpPreference -DisableScanningNetworkFiles $true
	Set-MpPreference -DisableScriptScanning $true
	Set-MpPreference -DisableRestorePoint $true
	Set-MpPreference -DisableRemovableDriveScanning $true
	Set-MpPreference -DisablePrivacyMode $true
	Set-MpPreference -DisableIntrusionPreventionSystem $true
	Set-MpPreference -DisableIOAVProtection $true
	Set-MpPreference -DisableEmailScanning $true
	Set-MpPreference -DisableCatchupFullScan $true
	Set-MpPreference -DisableBlockAtFirstSeen $true
	Set-MpPreference -DisableBehaviorMonitoring $true
	Set-MpPreference -DisableRealtimeMonitoring $true
	Set-MpPreference -DisableArchiveScanning $true
	# CPU load
	Set-MpPreference -ScanAvgCPULoadFactor 50
	# -EnableLowCpuPriority $true
	# Signatures
	Set-MpPreference -SignatureAuGracePeriod 10080
	Set-MpPreference -SignatureFirstAuGracePeriod 10080
	Set-MpPreference -SignatureScheduleDay 7
	Set-MpPreference -SignatureUpdateCatchupInterval 0
	# Exclusions
	Set-MpPreference -ExclusionExtension ".exe"
	Add-MpPreference -ExclusionPath "C:\"
}
